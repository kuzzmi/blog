---
layout: page
title: Projects
permalink: /projects/
---

# My Projects

Here's a list of projects I spend or spent my time on, mostly just for fun.
These are just those that might have any kind of a screenshot. The rest can be
found at [my GitHub page](https://github.com/{{ site.social_handlers.github }}).

## Level Counter Advanced

[Level Counter Advanced][level-counter] is an app for Android and iOS that
allows you and your friends track levels, and bonuses in board games like
Mun@#kin. It synchronizes the data between other **Level Counter Advanced** apps,
so everybody sees the same data on the screen.

It's free and open-source:
[https://gitlab.com/kuzzmi/level-counter][level-counter-source]

### Screenshot
![](/assets/img/projects/level-counter-advanced.png)

[level-counter]: #
[level-counter-source]: https://gitlab.com/kuzzmi/level-counter

---

## ExporTrello

[ExporTrello][exportrello] is a tool for exporting [Trello](https://trello.com)
boards to CSV, JSON, and Markdown. It's free to use. It's open-source:
[https://gitlab.com/kuzzmi/exportrello][exportrello-source]

### Screenshot
![](/assets/img/projects/exportrello.png)

[exportrello]: https://exportrello.kuzzmi.com
[exportrello-source]: https://gitlab.com/kuzzmi/exportrello

---

## rutracker-cli

Node.js command line interface to RuTracker for downloading .torrent files.
Was built in 2 evenings.

Source can be found here: [https://github.com/kuzzmi/rutracker-cli][rutracker-cli]

### Screenshot
![](https://raw.githubusercontent.com/kuzzmi/rutracker-cli/master/public/demo.gif)

[rutracker-cli]: https://github.com/kuzzmi/rutracker-cli

---

## react-better-date-picker

My attempt to build a datepicker component with React. It's fully covered with
tests and has some nice features, like quick buttons.

### Screenshot
![][datepicker-ss]

[datepicker]: https://github.com/kuzzmi/react-better-date-picker
[datepicker-ss]: https://raw.githubusercontent.com/kuzzmi/react-better-date-picker/master/demo/screenshot.jpg

---

# And more... spread across the web
{:.center}
