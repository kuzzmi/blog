preview:
	$S export LANG=C.UTF-8 ; bundle exec jekyll serve --incremental --lsi

serve:
	$S export LANG=C.UTF-8 ; bundle exec jekyll serve --incremental --drafts --lsi
