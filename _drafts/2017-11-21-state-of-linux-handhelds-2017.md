---
date:        2017-12-08 00:41:47 +0200
title:       "State of Linux Handhelds 2017"
tags:        [ linux, mobile, hanheld, productivity ]
comments:    true
description: >
    It's almost the end of 2017 and this post is about a state of Linux
    handheld devices market.
---

It's almost the end of 2017 and I believe it's a good time to summarize things
and extrapolate tendencies. Today let's talk about Linux handheld devices
market.

I made a research of the market of Linux-powered devices that are available
either for a purchase, or for a pre-order on crowd-funding websites.

The order of items in the list has no particular sorting.

## Librem 5

[Librem 5][] is the first device on this list. The main focus of it is your
security and privacy and it's all Linux and open hardware. So expect it having
neither proprietary crap nor bloatware.

Crowd-funding campaign ended not so long time ago, but they still accept
[preorders][librem 5], so if you like it - back it or pre-order a unit.

It is being developed by a company called [Purism][]. The company has already
developed some cool Linux hardware, like two nice laptops. They also ship their
own Linux distribution, which will be used in the smartphone too. (test it).

This smartphone will introduce a few unique features like hardware switches for
various communication channels, like GSM, or WiFi, or GPS etc. Another goal of
this project is to make a smartphone out of open and free soft- and hardware.

There are a few issues I see with this device though. The first one can become
a no-go for many people, is the outdated hardware. It's not clear yet, what
Purism plans to put into the case, but doing R&D now means that specs won't be
changed a lot. On the other side by not knowing the details right now we,
consumers, can expect some up-to-date hardware. By June 2018 Purism will send
dev-kits to developers and backers, so at that time specifications will be
almost carved in a stone. The second problem is lack of apps. There are not
that many apps, so we'll be relying heavily on a web browser, and usually it's
quite a tedious task, that has chances of being shipped unpolished.

I actually pre-ordered [Librem 5][] as I really want this project to succeed
and I can afford a toy that in the worst case will just lay on the shelf.

Advantages:
- hardware switches to turn all the external communication off
- Linux
- multiple distributions
- privacy-focused
- open hardware and software
- multiple distributions
- Bitcoin is accepted as a form of payment

Disadvantages:
- long time waiting
- most likely quite outdated hardware - 2017 phone in 2019 year
- crowd-funding page gets quite a few updates
- probably a bit overpriced device
- no hardware keyboard

Specs:
- CPU i.MX6 is confirmed as guaranteed fallback if i.MX8 doesn't work.
- Screen: most likely this will be a Full HD touchscreen
- Price: $599 (pre-order)
- Release date: January 2019 (first bulk shipping)
-

Actions;
- Test PureOS
- put links and specs for CPUs

## Gemini

I BOUGHT IT

https://vimeo.com/23909555

Next device on this list is one from [Planet Computers][] - [Gemini PDA][]. It
claims to be an Android PDA with Linux dual-boot and a QWERTY keyboard.

Keyboard is marketed as

There are no official statemens on what distribution it will support though,
but nevertheless a device with a nice keyboard and modern hardware nowadays is
a treasure.

Pre-order prices are

Advantages:
- Potentially good keyboard
- Linux (dual-boot though)
- 2:1 screen ratio
- Big battery
- fair price

Disadvantages:
- Not the best reputation of some of the management people
- Updates on Indiegogo are quite slow
- Android'ish hardware
- Lack of privacy

Specs:
- CPU: ??
- Storage: ?? Gb, SD card slot
- Screen: 5.7”, 2:1 ratio
- Speakers: dual side
- Ports: USB Type C for peripherals
- OS: Android, Linux dual-boot
- Connectivity: WiFi or WiFi+LTE
- Battery: ?? mAh, "two weeks of stand-by time / 12 hours of talk time"
- Dimensions: 17.1 cm x 8.0 cm x 1.35 cm
- Weight: 400g
- Price: $299 for WiFi version and $399 for WiFi+4G version
- Release date: January 2018 (first batch)

Actions
- write to Planet Computers about Linux
- put links to the videos on YouTube

## PocketCHIP

[PocketCHIP][pocket chip] is a very interesting Linux device made by
[C.H.I.P.][chip]. It's a

Advantages:
- very cheap
- QWERTY keyboard
- open source software and hardware

Disadvantages:
- keyboard is not comfortable to use
- no shipping to my country

Specs:
- Connectivity: WiFi b/g/n, Bluetooth 4.0
- CPU: 1Ghz C.H.I.P.
- Storage: 4Gb
- RAM: 512Mb

Pre-order price is $69. Release date: November 2017 (on hold)

## GPD Pocket

Next device on the list is a beast, when compared to the rest, it's a laptop
with expected performance in 7” format. It comes pre-installed with either
Windows or Ubuntu. That means, as you might expect, that you deal with laptop
hardware, so there are no limitations to what can be installed there as a main
OS.

As a unique device in this category it gets quite pricey, so not really
something that worth buying if you're not sure you need one.

It has a full QWERTY-keyboard, clit instead of touchpad, a few ports (what
ports).

Its size makes it the best compromise between a laptop and PDA. Yes, it's a
compromise, as it tries to be both and as we know *cough*Surface Pro*cough* if
try to do both you're worse at both.



__



I'm not a big fan of either Android or iOS, and many of my freshly developed
worflows are all around using command line interface.

I found just a few reasonably good articles and they were all way too
incomplete.



I'm [moving to Linux](link), I decided to migrate my mobile workflows to Linux
as well, and explored the market to get better understanding on what is out
there at the moment.

The problem I found immediately is the lack of any profitable demand led to
absence of any.

Going full Linux implies a very limited market th


[purism]: https://puri.sm [librem 5]: https://puri.sm/shop/librem-5/

[gemini pda]: https://www.indiegogo.com/projects/gemini-pda-android-linux-keyboard-mobile-device-phone
[planet computers]: http://planetcom.co.uk/

[pocket chip]: https://getchip.com/pages/pocketchip
[chip]: https://getchip.com/

[gpg pocket]: jjj

