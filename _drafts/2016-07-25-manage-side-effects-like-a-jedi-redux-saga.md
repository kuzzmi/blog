---
layout:      post
date:        2016-07-25 14:04:31 +0200
title:       "Manage side effects like a jedi: Redux-Saga"
tags:        [ react, redux, web development, javascript ]
comments:    true
description: >
    redux-saga library is a great thing, however some parts
    of it are still not that comprehensive. With this post I'll
    try to tell more about them from the "consumer" point of
    view and give some practical examples
---
*NOTE: Though most of this information can be found in the [official documentation][3], I found it difficult to wrap my head around the topic and with this post I'll try to add more practical examples and explain them from the "consumer" point of view, so this can hopefully help others to understand.*

The first association I had when I've heard the word "saga" for the first time as a term was the Star Wars stuff. A lot of time has passed since then, but when somebody says this word, the all-mighty jedi appears in my head.

And something like this appears:

![](/uploads/0640d123fd686a15996abf6a86bad1c3)

## So what is a saga?

> A Saga is a set of rules for routing a job to multiple collaborating parties.
>
> [**Clarifying the Saga pattern** by kellabyte][1]

This should tells us everything we need to understand what saga is and why it's a good idea to use it. In context of Flux, a perfect example of a job is a dispatched action.


Shortly, saga pattern **is** something that is called a middleware. [`redux-saga`][2] and the whole concept of the saga pattern allows us, in the context of Redux, to better separate reduce functions, which must be pure, from managing side effects, which can be impure functions, from defining a whole sequence of actions that must be dispatched.

Examples of great candidates to be run in sagas are:

* asynchronous actions
* accessing browser's local storage
* using Notifications API
* handling incoming messages through websocket
* dispatching an action in response to another action

## redux-saga

The main principle of saga is to route jobs. In Flux architecture, due to unidirectional data/actions flow, this is a pretty easy task: we just need to place some kind of an interceptor (middleware), watch for actions, do something and pass the action further. So in our case, saga as a pattern is just another middleware.

`redux-saga` gives you basically a set of helper functions to make your work with side effects "easier and better". As it's plugged into the processing of dispatched actions it also allows you to dispatch other actions to the store, thus making the side-effect management much more structured.

## Generators

`redux-saga` heavily uses ES6 generators, which are a nice thing to write asynchronous code. As they are something you need to know to use `redux-saga`, here are a few fairly basic things about generators you'll need.

The syntax of defining a generator looks like this:

{% highlight javascript %}
{% raw %}
function* func() {
}
{% endraw %}
{% endhighlight %}

When generator is called, it returns an iterator object. The iterator has a function called `next()`. Calling this function will execute the body of a generator it was derived from and return an object about generator execution status:

{% highlight javascript %}
{% raw %}
function* func() {
    console.log('hello');
}

const iter = func();
console.log(iter.next());

// hello
// Object { value: undefined, done: true }
{% endraw %}
{% endhighlight %}

Inside of a generator function you're able to use `yield` operator which will stop the execution of a generator body when iterator.next() is used:

{% highlight javascript %}
{% raw %}
function* func() {
    console.log('hello');
    yield;
    console.log('bye');
}

const iter = func();
console.log(iter.next());

// hello
// Object { value: undefined, done: false }

console.log(iter.next());

// bye
// Object { value: undefined, done: true }
{% endraw %}
{% endhighlight %}

You can also yield values from generator:

{% highlight javascript %}
{% raw %}
function* func() {
    yield 'hello';
}

const iter = func();
console.log(iter.next());
console.log(iter.next());

// Object { value: "hello", done: false }
// Object { value: undefined, done: true }
{% endraw %}
{% endhighlight %}

By using generators `redux-saga` checks the `done` flag and allows you to stop execution of them, as well as checking whether one is still running. Generators also work great

It's definitely worth reading further about generators. I enjoyed reading [this guide](http://gajus.com/blog/2/the-definitive-guide-to-the-javascript-generators).

## Examples

*All the following examples will use the default setup taken from the *Getting started* part of the [official documentation][3].*

The most repetitive pattern of using `redux-saga` I've noticed in my projects is when you want to asynchroniously load the data.

## Links:

[1]: http://kellabyte.com/2012/05/30/clarifying-the-saga-pattern/
[2]: https://github.com/yelouafi/redux-saga
[3]: http://yelouafi.github.io/redux-saga/
