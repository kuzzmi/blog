---
layout:      post
date:        2017-11-06 00:41:47 +0200
title:       "HDMI CEC with Raspberry Pi"
tags:        [ linux, raspberry pi, home automation ]
comments:    true
description: >
    A few tips on how to enhance your home cinema experience
    with HDMI CEC using Raspberry Pi with OSMC as an example
---
[HDMI CEC][5]

[CEC-o-matic][1] is a quite handy tool when you're trying to wire things up.

```
```

[1]: http://www.cec-o-matic.com/
[2]: http://libcec.pulse-eight.com/
[3]: https://home-assistant.io/components/hdmi_cec/
[4]: http://wiki.kwikwai.com/index.php?title=The_HDMI-CEC_bus
[5]: https://en.wikipedia.org/wiki/HDMI#Consumer_Electronics_Control_.28CEC.29

{% comment %}

## What is

{% endcomment %}
