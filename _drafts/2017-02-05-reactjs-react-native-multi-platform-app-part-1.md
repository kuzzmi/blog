---
layout:      post
date:        2017-02-05 06:32:40 +0200
title:       "React.js & React Native: Multi-platform app (Part 1)"
tags:        [ react, react-native, javascript ]
comments:    true
description: >
    With this post I start a series of posts about development
    of multi-platform applications  using React and React Native.
    In this part I'll cover the basics of initial project setup,
    we'll create a prototype of the application and convert one
    of the components to work on Android and iOS.
---
The purpose of this series is to provide a detailed overview of building multiplatform applications from scratch to a release stage. I will try to explain and give you ideas and solutions that usually work, so you can apply them or adopt to other needs.

As a result we'll build a quite simple yet complete multi-platform application called "Decision Maker". This application will be choosing randomly an item of a list of options, so in the simplest scenario user can use it to decide what to drink: tea or coffee.

This chapter will cover the following concepts:

* Initial configuration
* Drafting UI and logic
* Separating UI into parts
* Creating a first truly multiplatform component
* Launching our first multiplaform application on Android

## Configuration and initial setup

I'll try to keep the amount of tools as minimal as possible, but we have some mandatory tools to build our all-in-one solution.

Our toolkit starts with [React Native][3]. To quickly get started, follow the [link](https://facebook.github.io/react-native/docs/getting-started.html).

After following the "Getting Started" guide on React Native webpage, you'll get a fully working environment and `react-native` command line interface. At this point we can initialize our project basic structure by running:

```bash
> react-native init DecisionMaker
```

This will initialize your project and add some boilerplate code as well.

For bundling and building we'll be using [Webpack][1].

So, let's create the initial bundling configuration.

As we'll be dealing with multiple platforms it's a good idea to split our configuration into several parts. One part will respond for the most common configuration across _all_ files, the second one will be responsible for _dev_ configuration, and the last part contains separate files for each platform.

```javascript
defaults.js        // common parts
dev.js             // dev common
dist.js            // dist common
web.[dev|dist].js  // web app
ext.[dev|dist].js  // extension
desk.[dev|dist].js // desktop app
```

Before we dive into each separate configuration file, let's create the very basic version of what we need just to start our app.

Open `web.dev.js` and put the following there:

```
const webpack = require('webpack');
const path = require('path');

const defaults = require('./default.js');

const entry = {
    index: [
        'webpack-dev-server/client?http://0.0.0.0:8081',
        'webpack/hot/only-dev-server',
        path.join(__dirname, '../src/index.web.js'),
    ],
};

const distPath = path.join(__dirname, '../dist');
const srcPath  = path.join(__dirname, '../src');

module.exports = {
    entry,
    output: {
        path: distPath,
        filename: '[name].js'
    },
    paths: {
        srcPath,
        distPath
    },
    module: {
        loaders: [{
            test: /\\.jsx?$/,
            exclude: /node_modules/,
            loaders: [
                'react-hot',
                'babel',
            ],
        }, {
            test: /\\.scss$/,
            loaders: [
                'style',
                'css',
                'sass',
            ],
        }],
    },
};
```

Okay, let's install a few last pieces to get started:

```
npm install -D babel-core babel-preset-react-native babel-register webpack webpack-dev-server node-sass react-hot-loader babel-loader sass-loader css-loader style-loader
```

If the file `.babelrc` doesn't exist, create one with the following content:

```
{
    "presets": ["react-native"]
}
```

Now open your `package.json` and add the following to use `webpack-dev-server` for serving our webapp:

```
...
scripts: {
 ...,
    "start:web": "node node_modules\\webpack-dev-server\bin\\webpack-dev-server.js --config ./webpack/web.dev.js --hot --port 8081 --content-base dist/web/",
    ...
}
...
```

By this time we should be able to start our application in the development mode with Hot Module Replacement enabled on the `localhost:8081`. Now we can finally do some development.

## Building the first screen

First, as we'll be

[1]: https://webpack.js.org/
[2]: https://developer.android.com/studio/index.html
[3]: https://facebook.github.io/react-native/
