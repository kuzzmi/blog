---
layout:      post
date:        2017-11-28 00:36:36 +0200
title:       "5 Reasons To Give Linux A Try"
tags:        [ linux, software, os ]
comments:    true
image:       /assets/img/5-reasons-to-give-linux-a-try.png
description: >
    This time I want to share 5 reasons why I advocate for using Linux whenever
    I have a chance, and why you should give it a try too.
---
People hate changes. Not all of them of course, but most do - that's the human
nature. However, without a change you might miss something great. That's kinda
pity that nowadays people purchase PCs preloaded with macOS or with Windows
without knowing that there's something else available.

## 1. It's free

As simple as it is, all of the Linux distributions are free. You can get a
full operating system free of charge. You'll be able to find "paid" Linux
distributions, but they charge you not for the system itself, but for support
they provide.

Consider this: price of PCs or laptops you purchase includes the price for OS
licence. That means that if you buy hardware without Windows or macOS you save
a hundred or two bucks that you can spend on better hardware or software.
Profit? Profit!

Installing Linux on a freshly purchased machine is a matter of 10-15 minutes.
If you do some math, you find out, that you can save roughly $150 in 15
minutes, what results in $600/hr. So if you don't earn $600 hourly, then it's
worth giving a try.

Just visit [DistroWatch](http://distrowatch.com/), find a distribution you like
and download it for free.

## 2. It's more secure

> Two cowboys in the saloon are drinking whiskey. One of them is local, another
one is his friend from another city. Suddenly a man breaks in, starts shooting
and shouting very loudly and leaves. Nobody reacts.
> -- Billy?  
> -- Yes, Harry?  
> -- What was that?  
> -- That was Slippery Joe, Harry.  
> -- Does it mean nobody is able to catch him?  
> -- No, Harry. That's because nobody wants to.
>
> _A joke_

Anything human built is by no means 100% unhackable, let's be clear and agree
on this.

On one hand side Linux kernel (a core of the system) has so many eyes watching
it due to a nature of open source software, that chances of a security bug
sitting there for a long time are fairly low. On the other hand Linux users are
like Slippery Joes from the joke - they are a very small target group. This
means that hackers most likely choose another **cough** Windows **cough**
system to attack.

Most of the time, when you do update your system you can carefully pick and
choose what you update and install on your computer. That means that nobody in
the whole Universe will force you to update things that you do not want to
update right now **cough** Office **cough**.

## 3. It respects your privacy

Do you know that *some* companies get so freaking huge amount of the
information about you, that they know you better than your own mom?

I get amazed when people tell that they don't care about privacy because they
have nothing to hide. You can hear the same about security as well. Honestly,
I think that's bullshit. Neither privacy control, nor high security measures
are meant to be retrospective. They are something you do to _prevent_, not to
get rid of after this **bad** happened. They are protective and prospective
measures you take in order to not worry when you need to be in privacy.

Nobody enjoys being surrounded with surveillance cameras, but nobody cares when
the surveillance machine is just standing on their desks, that's kind of absurd,
isn't it?

In this regards Linux goes above and beyond that, the code is constantly
monitored not only for various security issues, but also to prevent this kind
of things. Popular Open Source software in general is a thing that gives
confidence in being respectful to your privacy.

With almost any Linux distribution you by default opt-out of any data
collection (if there are any), you get full freedom to pick and choose what to
install. Tools like [GnuPG](https://gnupg.org/) allow you to encrypt things in
a very easy manner and exchange different things with only those people who are
meant to receive them.

## 4. It's customizable

Do you know how much hustle it is to change the way of how your macOS or
Windows look? Tired of staring at the same everyday or you want something not
available in your interface?

For me personally the ability to configure whatever I want is one of the main
reasons to use Linux.

Take a look at these posts from
[/r/unixporn](https://www.reddit.com/r/unixporn):

{% include ref-screenshot.md screenshot="http://i.imgur.com/5otzCMo.jpg" source="https://www.reddit.com/r/unixporn/comments/6vjua2/gnome_they_said_linux_is_for_it_people_only_my/" %}

{% include ref-screenshot.md screenshot="https://i.redditmedia.com/TCsJ2ds6-Mx7Hs8CR_uu57c1ngjngPR4Up1VQHGOrFI.png?w=1024&s=0aedd1cacc0699eb06a463c0bc4214cb" source="https://www.reddit.com/r/unixporn/comments/7djwht/openbox_vinyl/" %}

{% include ref-screenshot.md screenshot="http://i.imgur.com/B1oroyH.jpg" source="https://www.reddit.com/r/unixporn/comments/367a7i/oc_flat_gnome_theme/" %}

{% include ref-screenshot.md screenshot="https://i.imgur.com/8F7MU2K.png" source="https://www.reddit.com/r/unixporn/comments/6xlmsf/herbstluftwm_gloomy_night/" %}

If you can do the same things in other OSes, that's great. But what amount of
effort it takes?

## 5. It's faster

This reason is debatable. However, the only OS that still gets updates and can
resurrect your old laptop is... Linux. Just because how the whole system works,
running Linux on your old machine doesn't require you to have the best hardware
to do basic internet browsing, office work, watching movies, listening to
music, even doing programming.

People almost through away laptops that are 3-4 years old just because they can
not run modern software there. I got a free laptop, because previous owner
couldn't use Firefox on it anymore. I installed [Xubuntu](https://xubuntu.org)
on it and that was it - a brand new, slightly outdated laptop ready to serve.

Remember a very brief era of netbooks? They all suck. The only way of making
them to suck less was to install some Linux distribution. This helped to reduce
resources, what resulted in far better performance overall.

With Windows and macOS you know what to expect. Each new version and update
require better hardware, in the end you're forced to buy a new PC or a new Mac.

Do you need more evidence? Read this amazing article by Bryan Lunduke: [Linux powers Supercomputers. (All of them.)](http://lunduke.com/2017/11/15/linux-powers-supercomputers-all-of-them/).

## Don't know where to start?

[DistroWatch](http://distrowatch.com/) is a good place to start, if you can
close your eyes on their design. It's pragmatic, but sucks very much. You can
pick any distribution from the list of popular ones.

In case you're still in doubt, you know nothing about Linux, I'd recommend
giving a try to one of these:

- [Elementary OS](https://elementary.io/)
- [Mint](https://linuxmint.com/)
- [Ubuntu](https://ubuntu.com/)

_I truly hope you found something useful in this article, if you did, please share it with your friends and colleagues._
