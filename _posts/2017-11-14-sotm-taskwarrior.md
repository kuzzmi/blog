---
layout:      sotm
date:        2017-11-14 11:13:05 +0200
title:       "SotM: TaskWarrior - Great CLI Task Manager"
name:        "TaskWarrior"
logo:        "http://lh3.googleusercontent.com/I6_8UWZI7hoh4NQ1QM4NaRrJfa_bSRgloOlQ2w_ADqelwUvTGJ1NsktlCgnByf7UHNk=w200"
link:        "https://taskwarrior.org"
comments:    true
type:        sotm
tags:
    - productivity
    - task managers
    - cli
    - software of the month
description: >
    Taskwarrior is an open-source, cross platform time and task management tool.
    It has a command-line interface rather than a graphical user interface.
screenshots:
    - https://inconsolation.files.wordpress.com/2014/05/2014-05-17-6m47421-taskwarrior.jpg
    - https://taskwarrior.files.wordpress.com/2013/12/color5.png
    - http://2.bp.blogspot.com/-18wCjpBzhhU/TXy0mlyl3aI/AAAAAAAADCQ/mE0osfUWU9c/s1600/ListingTaskWarrior.png
scores:
    - type: practicality
      score: 5
      comment: >
          Since the time I found it, the bar for "practical" software has
          been raised permanently
    - type: usability
      score: 5
      comment: >
          This is true CLI application, meaning that it can be used in
          variety of scenarios"
    - type: customization
      score: 5
      comment: >
          You can configure a lot of things, including colors, default and
          predefined settings
    - type: ui
      score: 4
      comment: >
          UI is great for CLI tool. There is always space for improvement and I personally wasn't astonished with it, so 4 out of 5 is a good compromise
    - type: coolfactor
      score: 3
      comment: >
          Whenever you caught using it, not everybody will go "WOAH!", only
          those who know what it is will
    - type: easeofuse
      score: 3
      comment: >
          It's not easy to set everything up, however it might require quite
          an effort to setup the server, so you can use it on the go
    - type: portability
      score: 4
      comment: >
          TaskWarrior uses JSON and stores everything in plain files
---
I found [TaskWarrior][1] a few weeks ago and to be honest - this
is by far the best task manager.

This tool is the first and only software that helped me in implementing my
workflow. _(I will write about this workflow in a separate post.)_

All the details about TaskWarrior you can find on their official [website][1],
so now I will try to summarize the reasoning why I think this program deserves
to be loved.

[1]: {{ page.link }}
