---
layout:      post
date:        2017-06-04 21:28:38 +0200
title:       "ErgoDox and Colemak Time"
tags:        [ keyboards, development ]
comments:    true
description: >
    I've finally switched to ErgoDox and Colemak and would
    like to share some of my thoughts on this.
---
comments:    true
**TL;DR** Yes that was hard and yes after 3 months of regular typing my typing
speed is still worse than before. However, I don't regret making that decision
and my typing _(coding)_ sessions can be now way longer without any issues.

*Small advice #1*: switch to your new layout wherever possible to make the transition smoother and faster
*Small advice #2*: don't switch when you have a deadline at work :>

## Reasons to Switch

Everybody should have their own reasons. The main reasons for me to switch were my health and comfort. **Not** a typing speed, as many might think.

When I was a kid I had broken my hand a few times and it took my hand 20 years to start reminding me about the fractures. So when I started feeling that long typing is no longer an option for me, I decided to switch to another layout.

## Picking the Layout

I'm always keen to try something unusual. And the keyboard layout wasn't an exception. As you might now, [QWERTY][3] wasn't designed for a comfortable typing experience using PC. If you are seeking for something that was designed specifically for that purpose you have a few other popular options: [Dvorak][2] and [Colemak][1].

_Just take a look how different they are (images from Wikipedia)_

![](/uploads/7f935629a3ed24093d3a18e72c268bfc)

I tried [Dvorak][2] several times, but it didn't work pretty well for me due to the fact that I'm Vim user and generally speaking it was very difficult to learn.

The difference between Dvorak and Colemak is that the former differs _very_ much from QWERTY, whereas the latter is 48% QWERTY-like, meaning that the most of hotkeys are on their places. At the same time, the buttons on the home row are very frequently used and you can type a lot of words just from it. So it seems like a great replacement for QWERTY.

That said Colemak was much more appealing to me: it is easier to start using and not to relearn **hjkl**, Ctrl+C and Ctrl+V combinations. *Not for the code of course :)*

So the layout of my choice clearly was Colemak.

Adoption period was horrible, to be honest, my productivity dropped almost to 0, as I always use cold turkey method of learning new stuff.

However, after 3 weeks, my typing speed climbed back to norm, thanks to [](https://www.keybr.com/):

![](/uploads/c0f8a8f68868b9cb47c26651695c7195)

It took me about 21 days (1 hour a day for 3 weeks) to get back to my usual typing speed. Quite a little price for the ability to type longer with much less effort. Especially, if you take into account that this happened during my transition to ErgoDox.

## Road to ErgoDox

It's been a while since I wanted to switch to an ergonomic mechanical keyboard. I've been searching for a good option for quite a long time.

I even started building my own keyboard, but managed to build only one of two parts of a split keyboard. I made a few mistakes in choosing materials and keys layout.

![](/uploads/cf584255d30a4a98915e75d22874f99e)

In order to understand what I need and how it must be built I decided to order ErgoDox from [Massdrop](https://www.massdrop.com/r/GF8XYU) website, so I can build it and see how it works for me.

After assembling I immediately switched to Colemak. I needed to adopt to a new keyboard anyway, so I decided to combine two things I had to get used to and cold turkey is the best way of doing that.

This is how my setup looks like after switching:

![](/uploads/9aaf19e70b5f04b8c0d855a671e6c225)

## Verdict

If I had an option to go back in time and to chose once again whether I want to switch or not, I would do this with no considerations.

Here is a list of why I would do this again:

1. Colemak itself is a great layout, I can't advocate for it enough. Going back to QWERTY is like "Who da hell could place commonly used together keys so far from each other?!". Vim requires another level of getting used to, but I haven't remapped any key with Colemak.
2. Learning a new skill forced my brain to activate some hidden resources. It's such a nice feeling that you are able to destroy and redevelop habbits.
3. ErgoDox placed all the keys in the position where your fingers naturally look for them. I went a bit further and moved numbers and characters like `<([{}])>` on the home row and can type comfortably without a lot of finger movement.
4. Having separate keys for your thumbs feels like magic. I had so much frustration with the original space bar, like "Why this key is so long?!".
5. It's another level of "geekness", a bit of IKEA effect of feeling that you've built a thing yourself.
6. Easy to experiment with new stuff, as I described in one of my [previous posts](https://kuzzmi.com/blog/no-numbers-symbols-first).

And of course there are some drawbacks of switching:

1. Time and energy consuming. You spend time on learning something that you already can do, so it's a bit difficult to justify the efforts in your head, what can affect motivation.
2. Your productivity drops. I made a mistake and made a transition during a deadline, what forced me to learn much faster, but led to much more stressful switching.

That's all. As you can see pros outweight cons, so I hope you can find something useful and convincing for yourself.

**Share your thoughts, experience and questions in the comments section**

[1]: https://colemak.com/
[2]: http://www.dvorak-keyboard.com/
[3]: https://en.wikipedia.org/wiki/QWERTY
