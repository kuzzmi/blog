---
layout:      post
date:        2017-11-14 9:54:09 +0200
title:       "Software of the Month"
tags:
    - software
    - linux
    - cli
comments:    true
description: >
    I decided to start a new post category, that will (hopefully)
    contain in the a list of the most useful/interesting software
    I used/found during the month.
---

I decided to start a new post category, that will (hopefully)
contain in the a list of the most useful/interesting software
I used/found during the month. It will be much easier to keep
it up-to-date, as it will force me to produce at least one
article in a month, thus helping me to keep this place alive.

I tend to love software that was developed for the command-line
usage, especially those made for Linux, but there might be some
exceptions.

Some of these things were developed a long time ago, some of
them might be developed recently, but in the end of the day
everything I will list in this category definitely caught my
attention.

I might add things retrospectively just to make sure I didn't
forget anything and to make it a nice collection.
