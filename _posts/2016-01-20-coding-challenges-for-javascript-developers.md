---
layout:      post
date:        2016-01-20 18:10:13 +0200
title:       "Coding Challenges for Javascript Developers"
tags:        [ problems solving, javascript, tips ]
comments:    true
description: >
    The list of competitive programming sites that allow you to use JavaScript
    to submit solutions
---
Here is the list of competitive programming sites that allow you to use JavaScript in order to submit your solution:

1. [HackerRank](https://www.hackerrank.com/) supports a lot of languages, has many active registered users (I believe something around 70-80k), smooth user experience and UI. Supports contests, splits problems in domains, so you can practice what you want and need. Recommended for JavaScript developers since you don't have a feeling of using old-school tools.
1. [CodeEval](https://www.codeeval.com/) has clunky UI/UX, not as comfortable as HackerRank and may confuse you with status of your submission. Apart from that it's good enough to give it a shot. The community is not that big, something around 10k I guess.
1. [CodeWars](http://www.codewars.com/) looks nice and feels nice. It's focused more on sharpening language skills, so not too many languages are supported now, but you still have a lot of interesting problems to solve. Interesting ranking system and you can compare your solution with others.
1. [SPOJ](http://www.spoj.com/) is also a well known place to practice your skills as well as participate and create in different contests. Haven't used too much, but looks good. A lot of people recommend it.
1. [CoderByte](https://coderbyte.com/challenges/). Personally I haven't used it, but it exists. At least this fact is worth of mentioning it :) Has not so many challenges.
1. [The Project Euler](http://projecteuler.net/) only requires you to enter the right answer, therefore there is no restriction on the language you use. Incredibly popular as far as I know.

I want to keep this list up-to-date, so if you know more online judging systems and/or coding challenging platforms that support JavaScript, please leave the comment below.
