---
layout:      post
date:        2016-03-12 22:47:01 +0200
title:       "5 Good Reasons To Start Streaming Your Coding"
tags:        [ streaming, development, motivation, tips, livecoding ]
comments:    true
description: >
    Benefits I have found during my streaming "career" and how streaming
    will help you to get things done. 5 benefits for streaming your side
    project development.
---
I've been streaming my coding for a couple of months, almost everyday, around three hours per day. That was my one of the most productive periods. And I'm going to return to streaming.

Although I'm not the most frequent and regular streamer on the services like [Livecoding.tv][lctv], I learned a lot from streaming some side projects development.

*I was even [interviewed](http://blog.livecoding.tv/2015/09/28/lctvalerts-web-service-kuzzmi-javascript-switzerland/) there.*

Me streaming on Livecoding.tv:

![Me streaming on Livecoding.tv](/uploads/d6c504398cdf0abb1bf8df7195378d25)


## So here is why you should start streaming your coding:

### 1. You stay focused

While streaming you have to be focused only on one task, on coding. You should be able to answer questions if your viewers have any. This doesn't take too much time and these questions more or less are related to what you're doing. So these questions can even help you if you're stuck. Streaming also prevents you from going to your favorite news website, or going to `/r/funny` or doing anything else.

In the best case you learn how to concentrate your attention on your project, on teaching others, on whatever else you want to stream. In the worst case you just stay concentrated during the session.

### 2. [Lernen durch Lehren](https://en.wikipedia.org/wiki/Learning_by_teaching) (Learning by teaching)

> #### When one teaches, two learn
>
> -- Robert A. Heinlein

A lot has been said about this topic. Most of you know that we learn a lot when we teach others, so streaming is just an extra tool to learn and teach. It allows you to learn about what you are doing. You can develop everything you want, but once in a while a cool guy comes and asks a good question. And you have to answer it *(of course if you're not a moron)*. If you don't know an answer, just go and dig for it.

### 3. Learn to plan

One of the first things you learn during streaming is to do this *on schedule*. You definitely can do this randomly, but doing so won't result in any amount of regular viewers and you will lose your motivation to stream.

You learn how to plan your streams and when to put them on a calendar. By doing this people interested in what you're doing will most likely visit your channel, giving you more motivation to do what you do.

*I broke my schedule due to long vacaction. And couldn't get back even by reducing amount of streaming sessions per week.*

### 4. Communicate

Sometimes you have to talk a lot about what you're doing. Preferably you should comment your actions. Thus you train to structure your thoughts, explain people why did you decided to pick this library among others etc. By doing this you understand more about what exactly you're doing and whether you do this right.

### 5. Archive your thoughts

On web services like [Livecoding][lctvc] you have a video archive, where you can find all your streamed videos. E.g. here is [mine](https://www.livecoding.tv/kuzzmi/videos/) (not all the videos I leaved there). Need to go back in time and understand why you put that ugly hack there? Just go to archive and check yourself!

## Moral

Often programmers suffer from procrastination and it's so tempting to distract on different things, like checking reddit, reading hacker news, or "incredibly useful article" about hamsters. Thus we can loose a lot of time doing nothing, pretending we do something useful etc. It happens even when it comes to our side projects. There are a lot of tools on the web that might help you to cut this distractions, to block different websites that you don't want to visit your development. It helps, but just temporarly.

If you want to acomplish your old side project, you should try to stream its development regularly and you won't even notice how you start getting things done.

**Once you start it, it's hard to stop.**

### Links:
* [Livecoding.tv][lctvc]
* [Twitch](https://www.twitch.tv/)

[lctvc]: https://www.livecoding.tv
[lctv]: https://www.livecoding.tv/kuzzmi
