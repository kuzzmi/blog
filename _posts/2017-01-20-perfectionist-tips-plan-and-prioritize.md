---
layout:      post
date:        2017-01-20 15:01:39 +0200
title:       "Perfectionist Tips: Plan and Prioritize"
tags:        [ productivity, psychology, perfectionist tip, products, startup ]
comments:    true
description: >
    "Perfectionist Tips" is a series of short stories and notes
    I wish I had read before getting burnt. In this post I write
    how a not critical bug killed all my enthusiasm and motivation
    to work on Whubi, a productivity product I built from scratch
    on my own.
---
When I started developing a productivity tool - [Whubi](https://whubi.com) - I was so enthusiastic about it and was so motivated to work on it beside my 40-hours-week for another 30-40 hours a week.

But then surprisingly I found myself in a trap that I basically created myself. That led my motivation down and even caused my brain to constantly finding all kinds of excuses to stay at home and try to finish all my stuff.

## The Idyll

Initially, I was trying to balance my work: a bit of development, a bit of marketing, a bit of user communication, some development operations, a bit of here and there. Everything was aligned perfectly and I wasn't overwhelmed with my tasks, so I could easily switch my activities if I feel that I got tired.

I was wearing all the hats and doing a lot of stuff, using my own creation for moving things forward and seeing areas of improvement.

I had all the needed metrics, so I could at least say to myself how well I'm doing.

I had a plan. I was trying to plan for not that far future and still to have high-level goals, which allowed the product to evolve in a more or less predictable way.

And then suddenly things started falling apart.

## The Bug

I was so enthusiastic to make everything perfect, that I started doing more work than I planned to bundle more features, fix more bugs and generally make my product even better. And instead of creating a good pressure, which is usually useful for a perfectionist, it created a bad pressure, when nothing in the world could help me to achieve my goals in a given amount of time.

I had a plan and a list of things I wanted to do, but one small bug got my attention and I couldn't let it get into the final build. Trying to fix this one bug led the other feature to be reworked a bit, what led to two other bugs, which led to reworking some other parts of the application and by the end of the planned time, the whole app was broken.

So all the **vital** fixes and improvements were not delivered. And this created, even more, pressure and I couldn't get out of this stuff.

Rule #1 for perfectionists:

> # Stick to the plan!
> Found a bug? See an urgent need for a feature? **Plan it**. Don't let it distract your attention!

Everything is relative and sometimes fixing a show-stopping bug will be a better idea than adding a nice-to-have feature.

Still, **priorities are the key**. In my case, everybody could live with a bug that I was trying to fix. Nobody actually reported about it.

### P.S.

[Whubi](https://whubi.com) is alive and is working fine with a few small glitches, however, it's in a forever-beta stage, which is another topic for the future post(s).

### Update - 01.10.2017

Unfortunatelly I couldn't manage to do all-sides development as quickly as it was required to keep the project alive, so Whubi went to the infinite holiday.
