---
layout: page
title: About
permalink: /about/
---

<div style="inline-block">
    <img style="margin: 0;" src="{{ site.email | gravatar }}" />
</div>

Hello, my name is Igor. I love my wife, Elm, Vim, and JavaScript. I also love cars and am now trying to get into [autosports](http://howtoautosport.com).

I'm somewhat social:

* My [Twitter](https://twitter.com/{{ site.social_handlers.twitter }})
* My [Facebook](https://facebook.com/{{ site.social_handlers.facebook }})
* My [GitLab](https://gitlab.com/{{ site.social_handlers.gitlab }})
* My [GitHub](https://github.com/{{ site.social_handlers.github }})
* My [LinkedIn](https://linkedin.com/in/{{ site.social_handlers.linkedin }})
* My [Email](http://www.google.com/recaptcha/mailhide/d?k=01u9w1IrkGbjUo7HO21LHZnw==&c=Z6ZmOi-m7Z0AJ4pmruD3Mw==)

Here's my [GnuPG public key](/assets/igorkuzmenko.asc) in case you need it.
