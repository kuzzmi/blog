# This file is licensed under the MIT License (MIT) available on
# http://opensource.org/licenses/MIT.

require 'yaml'
require 'slugify'

module Jekyll

    class PostsPerTagPage < Page
        def initialize(site, base, tag)
            @site = site
            @base = base
            @dir = File.join('blog', 'tags', tag.slugify)
            @name = 'index.html'

            self.process(@name)
            self.read_yaml(File.join(base, '_layouts'), 'tag.html')
            self.data['tag'] = tag
        end
    end

    class PostsPerTagPageGenerator < Generator
        safe true

        def generate(site)
            # Get the collection of wallets from _wallets
            posts = site.posts;

            allTags = []

            posts.docs.each do |post|
                post.data['tags'].each do |tag|
                    next if allTags.include? tag
                    allTags.push(tag)
                    site.pages << PostsPerTagPage.new(site, site.source, tag)
                end
            end

        end
    end

end

